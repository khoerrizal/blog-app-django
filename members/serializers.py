# serializers.py
from rest_framework import serializers
from .models import Members


class HeroSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Members
        fields = ('id', 'firstname', 'lastname')
