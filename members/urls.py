from django.urls import path
from . import views
from django.urls import path, include

# swagger
from drf_yasg import openapi
from drf_yasg.views import get_schema_view as swagger_get_schema_view

schema_view = swagger_get_schema_view(
    openapi.Info(
        title="Member API",
        default_version="1.0.0",
        description="Swagger Documentation"
    )
)

urlpatterns = [
    path('', views.index, name='index'),
    path('hello-world', views.hello_world, name='hello_world'),
    path('first-page', views.first_page, name='first_page'),
    path('raw-data', views.raw_data, name='raw_data'),
    path('add/', views.add, name='add'),
    path('add/addrecord/', views.addrecord, name='addrecord'),
    path('delete/<int:id>', views.delete, name='delete'),
    path('update/<int:id>', views.update, name='update'),
    path('update/updaterecord/<int:id>', views.updaterecord, name='updaterecord'),
    path('api-doc', schema_view.with_ui('swagger', cache_timeout=0), name='swagger-schema')
]
